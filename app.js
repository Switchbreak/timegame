window.addEventListener('DOMContentLoaded', function() {
    var canvas = document.getElementById( 'renderCanvas' );
    var engine = new BABYLON.Engine(canvas, true);

    var createScene = function() {
        var scene = new BABYLON.Scene(engine);

        var camera = new BABYLON.FollowCamera('camera1', new BABYLON.Vector3(0, 0, -40), scene);

        var light = new BABYLON.PointLight('light1', new BABYLON.Vector3(0, 0, -5), scene);
        light.intensity = 0.3;

        var room = BABYLON.Mesh.CreateBox('box1', 16, scene, false, BABYLON.Mesh.BACKSIDE);

        var spriteManager = new BABYLON.SpriteManager('playerSpriteMgr', 'img/fencer-male.png', 1, 64, scene);
        var player = new BABYLON.Sprite('player', spriteManager);

        scene.actionManager = new BABYLON.ActionManager(scene);
        scene.actionManager.registerAction(new BABYLON.IncrementValueAction({ trigger: BABYLON.ActionManager.OnKeyDownTrigger, parameter: "w" }, player, 'position.y', 0.1));
        scene.actionManager.registerAction(new BABYLON.IncrementValueAction({ trigger: BABYLON.ActionManager.OnKeyDownTrigger, parameter: "a" }, player, 'position.x', -0.1));
        scene.actionManager.registerAction(new BABYLON.IncrementValueAction({ trigger: BABYLON.ActionManager.OnKeyDownTrigger, parameter: "s" }, player, 'position.y', -0.1));
        scene.actionManager.registerAction(new BABYLON.IncrementValueAction({ trigger: BABYLON.ActionManager.OnKeyDownTrigger, parameter: "d" }, player, 'position.x', 0.1));

        player.rotation = new BABYLON.Vector3.Zero();
        camera.rotationOffset = 0;
        camera.heightOffset = 0;
        camera.target = player;

        return scene;
    }

    var scene = createScene();

    engine.runRenderLoop( function() {
        scene.render();
    });

    window.addEventListener('resize', function() {
        engine.resize();
    });
});
